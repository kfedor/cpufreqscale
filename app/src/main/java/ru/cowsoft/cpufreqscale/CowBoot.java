package ru.cowsoft.cpufreqscale;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.io.IOException;

import static ru.cowsoft.cpufreqscale.MainActivity.PREF_AUTOSTART;
import static ru.cowsoft.cpufreqscale.MainActivity.PREF_GOVERNOR;
import static ru.cowsoft.cpufreqscale.MainActivity.mypreference;
import static ru.cowsoft.cpufreqscale.MainActivity.setGovernor;

public class CowBoot extends BroadcastReceiver
{
    @Override
    public void onReceive( Context context, Intent intent )
    {
        SharedPreferences sharedpreferences = context.getSharedPreferences( mypreference, Context.MODE_PRIVATE );

        boolean autostart = false;

        if( sharedpreferences.contains( PREF_AUTOSTART ) )
        {
            autostart = sharedpreferences.getBoolean( PREF_AUTOSTART, false );
        }

        if( !autostart )
        {
            return;
        }

        if( intent.getAction().equals( Intent.ACTION_BOOT_COMPLETED ) )
        {
            if( sharedpreferences.contains( PREF_GOVERNOR ) )
            {
                try
                {
                    String gov = sharedpreferences.getString( PREF_GOVERNOR, "conservative" );
                    setGovernor( gov );
                    Toast.makeText( context, "CPU Governor: " + gov , Toast.LENGTH_SHORT ).show();
                }
                catch( IOException e )
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
