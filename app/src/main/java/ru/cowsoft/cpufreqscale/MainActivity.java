package ru.cowsoft.cpufreqscale;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity
{
    public static final String mypreference = "cowfreq";
    //final static String POLICY_DIR = "/sys/devices/system/cpu/cpufreq/policy0";
    final static String POLICY_DIR = "/sys/devices/system/cpu/cpu0/cpufreq";
    final static String AVAIL_GOVERNORS = "scaling_available_governors";
    final static String GOVERNOR = "scaling_governor";

    private SharedPreferences sharedpreferences;
    public static final String PREF_AUTOSTART = "autostart";
    public static final String PREF_GOVERNOR = "governor";

    private boolean isRoot = false;
    RadioGroup radioGroup;
    HashMap<Integer, String> governors_map = new HashMap<>();

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        sharedpreferences = getSharedPreferences( mypreference, Context.MODE_PRIVATE );

        setContentView( R.layout.activity_main );

        // Проверим рута
        isRoot = checkRoot();

        // Начинаем настраивать интерфейс
        radioGroup = findViewById( R.id.radiogroup );
        radioGroup.setOnCheckedChangeListener( new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged( RadioGroup radioGroup, int i )
            {
                try
                {
                    String gov = governors_map.get( i );
                    setGovernor( gov );

                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString( PREF_GOVERNOR, gov );
                    editor.apply();
                }
                catch( IOException e )
                {
                    e.printStackTrace();
                }
            }
        } );

        // Автостарт
        CheckBox checkbox_autoStart = findViewById( R.id.check_autostart );

        boolean autostart = false;
        if( sharedpreferences.contains( PREF_AUTOSTART ) )
        {
            autostart = sharedpreferences.getBoolean( PREF_AUTOSTART, false );
        }
        checkbox_autoStart.setChecked( autostart );

        // Если рут то можно включать автостарт
        if( isRoot )
        {
            checkbox_autoStart.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged( CompoundButton compoundButton, boolean b )
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putBoolean( PREF_AUTOSTART, b );
                    editor.apply();
                }
            } );
        }
        else
        {
            checkbox_autoStart.setClickable( false );
        }

        // Настраиваем радиокнопки
        String governor = readCurrentGovernor();
        readGovernors( governor );
    }

    static public void setGovernor( String gov ) throws IOException
    {
        String runcmd = "echo " + "\"" + gov + "\"" + " > " + POLICY_DIR + "/" + GOVERNOR;

        Runtime.getRuntime().exec( new String[]{ "su", "-c", runcmd } );
    }

    public void addRadioButton( String text, String current_gov )
    {
        RadioButton rdbtn = new RadioButton( this );
        rdbtn.setText( text );

        if( !isRoot )
        {
            rdbtn.setClickable( false );
        }

        radioGroup.addView( rdbtn );

        governors_map.put( rdbtn.getId(), text );

        if( text.equals( current_gov ) )
        {
            radioGroup.check( rdbtn.getId() );
        }
    }

    private String readCurrentGovernor()
    {
        File file = new File( POLICY_DIR, GOVERNOR );
        String line;

        try
        {
            BufferedReader br = new BufferedReader( new FileReader( file ) );

            line = br.readLine();

            if( line != null )
            {
                return line;
            }

            br.close();
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }

        return "";
    }

    private void readGovernors( String current_gov )
    {
        File file = new File( POLICY_DIR, AVAIL_GOVERNORS );

        try
        {
            BufferedReader br = new BufferedReader( new FileReader( file ) );
            String line;

            line = br.readLine();

            if( line != null )
            {
                String[] governors = line.split( "\\s+" );

                for( String t : governors )
                {
                    addRadioButton( t, current_gov );
                }
            }

            br.close();
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }

    private boolean checkRoot()
    {
        Process p;
        try
        {
            // Preform su to get root privledges
            p = Runtime.getRuntime().exec( "su" );

            // Attempt to write a file to a root-only
            DataOutputStream os = new DataOutputStream( p.getOutputStream() );
            os.writeBytes( "echo \"Do I have root?\" >/system/sd/temporary.txt\n" );

            // Close the terminal
            os.writeBytes( "exit\n" );
            os.flush();

            try
            {
                p.waitFor();
                if( p.exitValue() != 255 )
                {
                    return true;
                }
            }
            catch( InterruptedException ignored )
            {
            }
        }
        catch( IOException ignored )
        {
        }

        return false;
    }
}